import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginCofideCorreoPage } from './login-cofide-correo.page';

const routes: Routes = [
  {
    path: '',
    component: LoginCofideCorreoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginCofideCorreoPageRoutingModule {}
