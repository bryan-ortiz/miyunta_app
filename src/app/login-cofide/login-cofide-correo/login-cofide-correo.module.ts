import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginCofideCorreoPageRoutingModule } from './login-cofide-correo-routing.module';

import { LoginCofideCorreoPage } from './login-cofide-correo.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    IonicModule,
    LoginCofideCorreoPageRoutingModule,
  ],
  declarations: [LoginCofideCorreoPage],
})
export class LoginCofideCorreoPageModule {}
