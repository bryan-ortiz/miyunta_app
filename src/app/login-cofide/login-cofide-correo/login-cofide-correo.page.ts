import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login-cofide-correo',
  templateUrl: './login-cofide-correo.page.html',
  styleUrls: ['./login-cofide-correo.page.scss'],
})
export class LoginCofideCorreoPage implements OnInit {
  constructor(
    public router: Router,
    public loadingController: LoadingController
  ) {}

  ngOnInit() {}
  entrarCuenta() {
    this.handleButtonClick();
    setTimeout(() => {
      this.router.navigateByUrl('/dashboard-cofide');
    }, 3000);
  }

  entraRegistro() {
    this.router.navigateByUrl('/register-cofide');
  }

  async handleButtonClick() {
    const loading = await this.loadingController.create({
      message: 'Espere un momento ...',
      duration: 3000,
    });

    await loading.present();

    /*     this.router.navigateByUrl('/dashboard-cofide');
     */
  }
}
