import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginCofidePage } from './login-cofide-preview.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '**',
    pathMatch: 'full',
  },
  {
    path: '',
    component: LoginCofidePage,
    children: [
      /*  {
        path: 'register-cofide',
        loadChildren: () =>
          import('../register-cofide/register-cofide.module').then(
            (m) => m.RegisterCofidePageModule
          ),
      },
      {
        path: 'correo-cofide',
        loadChildren: () =>
          import('../login-cofide-correo/login-cofide-correo.module').then(
            (m) => m.LoginCofideCorreoPageModule
          ),
      }, */
    ],
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginCofidePageRoutingModule {}
