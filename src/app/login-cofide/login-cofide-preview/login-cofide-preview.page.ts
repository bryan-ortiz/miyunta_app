import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-cofide-preview',
  templateUrl: './login-cofide-preview.page.html',
  styleUrls: ['./login-cofide-preview.page.scss'],
})
export class LoginCofidePage implements OnInit {
  constructor(public router: Router) {}

  ngOnInit() {}
  overlayHidden: boolean = false;
  public hideOverlay() {
    this.overlayHidden = true;
  }

  entrarLogin() {
    this.router.navigateByUrl('/dashboard-cofide');
  }
  entraCorreo() {
    this.router.navigate(['/correo-cofide']);
  }
}
