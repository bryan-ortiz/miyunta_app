import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginCofidePageRoutingModule } from './login-cofide-preview-routing.module';

import { LoginCofidePage } from './login-cofide-preview.page';
import { ComponentsModule } from './../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    LoginCofidePageRoutingModule,
  ],
  declarations: [LoginCofidePage],
})
export class LoginCofidePageModule {}
