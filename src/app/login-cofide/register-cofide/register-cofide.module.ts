import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterCofidePageRoutingModule } from './register-cofide-routing.module';

import { RegisterCofidePage } from './register-cofide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterCofidePageRoutingModule
  ],
  declarations: [RegisterCofidePage]
})
export class RegisterCofidePageModule {}
