import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterCofidePage } from './register-cofide.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterCofidePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterCofidePageRoutingModule {}
