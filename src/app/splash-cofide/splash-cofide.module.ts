import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SplashCofidePageRoutingModule } from './splash-cofide-routing.module';

import { SplashCofidePage } from './splash-cofide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SplashCofidePageRoutingModule
  ],
  declarations: [SplashCofidePage]
})
export class SplashCofidePageModule {}
