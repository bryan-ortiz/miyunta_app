import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SplashCofidePage } from './splash-cofide.page';

const routes: Routes = [
  {
    path: '',
    component: SplashCofidePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SplashCofidePageRoutingModule {}
