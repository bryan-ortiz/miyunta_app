import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-splash-cofide',
  templateUrl: './splash-cofide.page.html',
  styleUrls: ['./splash-cofide.page.scss'],
})
export class SplashCofidePage implements OnInit {
  constructor(public router: Router) {
    setTimeout(() => {
      this.router.navigateByUrl('preloader');
    }, 3000);
  }

  ngOnInit() {}
}
