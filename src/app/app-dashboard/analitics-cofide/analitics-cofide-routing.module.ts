import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnaliticsCofidePage } from './analitics-cofide.page';

const routes: Routes = [
  {
    path: '',
    component: AnaliticsCofidePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnaliticsCofidePageRoutingModule {}
