import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-analitics-cofide',
  templateUrl: './analitics-cofide.page.html',
  styleUrls: ['./analitics-cofide.page.scss'],
})
export class AnaliticsCofidePage implements OnInit {
  constructor(public loadingController: LoadingController) {}

  ngOnInit() {}

  async handleButtonClick() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 3000,
    });

    await loading.present();
  }

  llamar() {
    this.handleButtonClick();
  }
}
