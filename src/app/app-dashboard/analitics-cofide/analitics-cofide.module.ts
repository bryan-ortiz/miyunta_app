import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnaliticsCofidePageRoutingModule } from './analitics-cofide-routing.module';

import { AnaliticsCofidePage } from './analitics-cofide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AnaliticsCofidePageRoutingModule
  ],
  declarations: [AnaliticsCofidePage]
})
export class AnaliticsCofidePageModule {}
