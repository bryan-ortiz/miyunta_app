import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeCofidePageRoutingModule } from './home-cofide-routing.module';

import { HomeCofidePage } from './home-cofide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeCofidePageRoutingModule
  ],
  declarations: [HomeCofidePage]
})
export class HomeCofidePageModule {}
