import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home-cofide',
  templateUrl: './home-cofide.page.html',
  styleUrls: ['./home-cofide.page.scss'],
})
export class HomeCofidePage implements OnInit {
  constructor(private menu: MenuController) {}

  ngOnInit() {}

  overlayHidden: boolean = false;
  public hideOverlay() {
    this.overlayHidden = true;
  }
  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }
  noFunka() {
    this.menu.open('');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
}
