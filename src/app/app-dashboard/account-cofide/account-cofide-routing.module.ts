import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountCofidePage } from './account-cofide.page';

const routes: Routes = [
  {
    path: '',
    component: AccountCofidePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountCofidePageRoutingModule {}
