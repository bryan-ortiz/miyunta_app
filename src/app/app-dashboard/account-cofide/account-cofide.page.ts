import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account-cofide',
  templateUrl: './account-cofide.page.html',
  styleUrls: ['./account-cofide.page.scss'],
})
export class AccountCofidePage implements OnInit {
  constructor(public router: Router) {}

  ngOnInit() {}

  entraTest() {
    this.router.navigateByUrl('/ingre-mensual');
  }
}
