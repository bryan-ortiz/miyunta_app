import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccountCofidePageRoutingModule } from './account-cofide-routing.module';

import { AccountCofidePage } from './account-cofide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccountCofidePageRoutingModule
  ],
  declarations: [AccountCofidePage]
})
export class AccountCofidePageModule {}
