import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardCofidePage } from './dashboard-cofide.page';

const routes: Routes = [
  /*  {
    path: '',
    component: DashboardCofidePage
  } */
  {
    path: '',
    redirectTo: 'home-cofide',
    pathMatch: 'full',
  },
  {
    path: '',
    component: DashboardCofidePage,
    children: [
      {
        path: 'home-cofide',
        loadChildren: () =>
          import('../home-cofide/home-cofide.module').then(
            (m) => m.HomeCofidePageModule
          ),
      },

      {
        path: 'analitics-cofide',
        loadChildren: () =>
          import('../analitics-cofide/analitics-cofide.module').then(
            (m) => m.AnaliticsCofidePageModule
          ),
      },
      {
        path: 'account-cofide',
        loadChildren: () =>
          import('../account-cofide/account-cofide.module').then(
            (m) => m.AccountCofidePageModule
          ),
      },
    ],
  },
  {
    path: '',
    redirectTo: '/dashboard-cofide/home-cofide',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardCofidePageRoutingModule {}
