import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardCofidePageRoutingModule } from './dashboard-cofide-routing.module';

import { DashboardCofidePage } from './dashboard-cofide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardCofidePageRoutingModule
  ],
  declarations: [DashboardCofidePage]
})
export class DashboardCofidePageModule {}
