import { Component, Input, HostBinding } from '@angular/core';
import { css } from 'emotion';

@Component({
  selector: 'keyboard',
  template: `
    <div *ngFor="let set of characters" [ngClass]="buttonsContainerStyles">
      <button
        *ngFor="let char of set"
        [id]="char"
        (click)="enterCharacter(char)"
        [ngClass]="buttonStyles"
      >
        {{ char }}
      </button>
    </div>
    <button (click)="enterCharacter(' ')">Space</button>
  `,
})
export class KeyboardComponent {
  @HostBinding('class') keyboardStyles = keyboardStyles;
  @Input()
  lastInput: HTMLInputElement;
  @Input()
  cursorPosition: number;
  characters = [
    ['7', '8', '9', '/'],
    ['4', '5', '6', '*'],
    ['1', '2', '3', '+'],
    ['. ', '0', '.', '+'],
  ];

  enterCharacter(char: string) {
    if (this.lastInput) {
      const chars = this.lastInput.value.split('');
      chars.splice(this.cursorPosition, 0, char);
      this.lastInput.value = chars.join('');
      this.lastInput.setSelectionRange(
        this.cursorPosition + 1,
        this.cursorPosition + 1
      );
      this.lastInput.focus();
    }
  }
}
/**** styles ****/
const keyboardStyles = css`
  display: flex;
  flex-direction: column;
  align-items: center;

  > button {
    width: 60vw;
  }
`;
const buttonsContainerStyles = css`
  display: flex;
  justify-content: center;

  button {
    width: 30pt;

    &:hover {
      cursor: pointer;
    }
  }
`;
Object.assign(KeyboardComponent.prototype, {
  buttonsContainerStyles,
});
