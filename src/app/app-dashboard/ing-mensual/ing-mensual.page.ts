import { Component, HostListener, OnInit } from '@angular/core';
import { css } from 'emotion';

const inputContainerStyles = css`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 10vh;
  background: lavender;

  input {
    height: 4vh;
    width: 80vw;
    border: 1px solid black;
    border-radius: 5px;
    font-size: 20pt;
    text-align: center;
  }
`;
@Component({
  selector: 'app-ing-mensual',
  templateUrl: './ing-mensual.page.html',
  styleUrls: ['./ing-mensual.page.scss'],
})
export class IngMensualPage implements OnInit {
  constructor() {}

  ngOnInit() {}
  inputContainerStyles = inputContainerStyles;
  lastInput;
  cursorPosition;
  @HostListener('focusout', ['$event'])
  onFocusOut(event) {
    event.target.tagName === 'INPUT' &&
      (this.lastInput = event.target) &&
      (this.cursorPosition = event.target.selectionStart);
  }
}
