import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IngMensualPage } from './ing-mensual.page';

const routes: Routes = [
  {
    path: '',
    component: IngMensualPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngMensualPageRoutingModule {}
