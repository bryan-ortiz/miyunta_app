import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IngMensualPageRoutingModule } from './ing-mensual-routing.module';

import { IngMensualPage } from './ing-mensual.page';
import { KeyboardComponent } from './keyboard.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,

    IngMensualPageRoutingModule,
  ],
  declarations: [IngMensualPage, KeyboardComponent],
})
export class IngMensualPageModule {}
