import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreloaderCofidePage } from './preloader-cofide.page';

const routes: Routes = [
  {
    path: '',
    component: PreloaderCofidePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreloaderCofidePageRoutingModule {}
