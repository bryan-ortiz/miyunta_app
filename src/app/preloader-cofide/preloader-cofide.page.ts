import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-preloader-cofide',
  templateUrl: './preloader-cofide.page.html',
  styleUrls: ['./preloader-cofide.page.scss'],
})
export class PreloaderCofidePage implements OnInit {
  constructor(public router: Router) {}

  ngOnInit() {}

  EntraLogin() {
    this.router.navigateByUrl('/login');
  }
}
