import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreloaderCofidePageRoutingModule } from './preloader-cofide-routing.module';

import { PreloaderCofidePage } from './preloader-cofide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreloaderCofidePageRoutingModule
  ],
  declarations: [PreloaderCofidePage]
})
export class PreloaderCofidePageModule {}
