import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

import { WelcomeMessageComponent } from './welcome-message/welcome-message.component';

@NgModule({
  declarations: [WelcomeMessageComponent],
  exports: [WelcomeMessageComponent],
  imports: [CommonModule, IonicModule],
})
export class ComponentsModule {}
