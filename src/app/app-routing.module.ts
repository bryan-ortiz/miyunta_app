import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  /*  {
    path: '',
    loadChildren: () =>
      import('./tabs/tabs.module').then((m) => m.TabsPageModule),
  }, */
  {
    path: '',
    loadChildren: () =>
      import('./splash-cofide/splash-cofide-routing.module').then(
        (m) => m.SplashCofidePageRoutingModule
      ),
  },
  {
    path: 'preloader',
    loadChildren: () =>
      import('./preloader-cofide/preloader-cofide-routing.module').then(
        (m) => m.PreloaderCofidePageRoutingModule
      ),
  },
  {
    path: 'login',
    loadChildren: () =>
      import(
        './login-cofide/login-cofide-preview/login-cofide-preview.module'
      ).then((m) => m.LoginCofidePageModule),
  },
  {
    path: 'dashboard-cofide',
    loadChildren: () =>
      import('./app-dashboard/dashboard-cofide/dashboard-cofide.module').then(
        (m) => m.DashboardCofidePageModule
      ),
  },
  {
    path: 'register-cofide',
    loadChildren: () =>
      import('./login-cofide/register-cofide/register-cofide.module').then(
        (m) => m.RegisterCofidePageModule
      ),
  },
  {
    path: 'correo-cofide',
    loadChildren: () =>
      import(
        './login-cofide/login-cofide-correo/login-cofide-correo.module'
      ).then((m) => m.LoginCofideCorreoPageModule),
  },
  {
    path: 'ing-mensual',
    loadChildren: () =>
      import('./app-dashboard/ing-mensual/ing-mensual.module').then(
        (m) => m.IngMensualPageModule
      ),
  },
  {
    path: 'ingre-mensual',
    loadChildren: () =>
      import('./app-dashboard/ing-mensual/ing-mensual.module').then(
        (m) => m.IngMensualPageModule
      ),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
